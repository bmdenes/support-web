<?php session_start(); ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>QuickHelp - Company</title>
		<?php include 'pages/meta_heading.php'; ?>
	</head>

	<body>
		<div id="main">
			<?php include 'pages/header.php'; ?>
			
			<div id="site_content">
				<?php include 'pages/sidebar.php'; ?>
				
				<div id="content">
					<h1>Register a company:</h1>
					
					<?php
						require 'includes/utils.php';
						require 'includes/Company.php';
					
						if(isset($_POST['register']))
						{
							if(isset($_SESSION["userid"]))
							{
								$name = test_input($_POST['name']);
								$phone = test_input($_POST['phone']);
								$address = test_input($_POST['address']);
								$description = test_input($_POST['description']);
								$company = new Company($name, $phone, $address, $description);

								if($company->register())
								{
									echo "<p>Registration has been completed successfully.</p>";
									log_message("register", "Company from " . $_SERVER['REMOTE_ADDR'] . " @ " . $name);
								}
								else
								{
									echo "<p>Error! Could not register the company as it already exists.</p>";
								}
							}
							else
							{
								echo "<p>Error! You need to be logged in to register a company.</p>";
							}
						}
						else
						{
							echo '<p>Here you can register the company to gain access at the support system.</p>
								<form action="register.php" method="post">
									<div class="form_settings">
										<p><span>Name</span><input class="contact" type="name" name="name"></p>
										<p><span>Phone</span><input class="contact" type="phone" name="phone">
										<p><span>Address</span><input class="contact" type="address" name="address">
										<p><span>Description</span><textarea class="contact textarea" rows="8" cols="50" name="description"></textarea>
										<p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="register" value="Register" /></p>
									</div>
								</form>';
						}
						
						echo '<h1>Registered companies:</h1>';
						include 'pages/show_companies.php';
						
						$conn->close();
					?>
				</div>
			</div>
			
			<?php include 'pages/footer.php'; ?>
		</div>
	</body>
</html>