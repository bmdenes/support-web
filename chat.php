<!DOCTYPE HTML>
<html>
	<head>
		<title>QuickHelp - Live</title>
		<?php include 'pages/meta_heading.php'; ?>
		
		<!-- Chat styling -->
		<!-- Most styling is developer controlled -->
		<link href="style/chat.css" rel="stylesheet" type="text/css">
		  
		<!-- Gateway -->  
		<script src="javascript/gateway.js" type="text/javascript"></script>

		<!-- Application -->
		<script type="text/javascript">  
		// Kaazing client ID
		var KAAZING_ID = "8381c16e-11f4-4bac-9ae7-c03a2c9376ec";  

		// Called when the window is loaded
		// Creates chat infrastructure
		function doWindowLoad()
		{
		  // Utility method on library
		  // Client ID
		  // Input DIV element
		  // Output/history DIV element
		  Gateway.chat( 
			KAAZING_ID, 
			document.querySelector( ".message" ), 
			document.querySelector( ".history" )
		  );    
		}
		  
		// Event listener for when loaded
		window.addEventListener( "load", doWindowLoad );  
		</script>  
	</head>

	<body>
		<div id="main">
			<?php include 'pages/header.php'; ?>
			
			<div id="site_content">
				<?php include 'pages/sidebar.php'; ?>
				
				<div id="content">
					<h1>Live Support</h1>
					<p>Real time communication using the support system's chat.</p>
					<div class="history">
						<div style="color: #000">Trying to establish the connection...</div>
					</div>
					<div class="message">Press &lt;Enter&gt; to send.</div>
				</div>
			</div>
			
			<?php include 'pages/footer.php'; ?>
		</div>
	</body>
</html>