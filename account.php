<?php session_start(); ?>
<!DOCTYPE HTML>
<html>
	<head>
	  <title>QuickHelp - Account</title>
	  <?php include 'pages/meta_heading.php'; ?>
	</head>

	<body>
		<div id="main">
			<?php include 'pages/header.php'; ?>
			
			<div id="site_content">
				<?php include 'pages/sidebar.php'; ?>
				
				<div id="content">
				<?php
					require 'includes/utils.php';
					require 'includes/Account.php';
					
					if(!isset($_SESSION["userid"]))
					{
						if(isset($_POST["login"]))
						{
							echo '<h1>Log-In</h1>';

							$email = test_input($_POST['email']);
							$password = test_input($_POST['password']);

							$account = new Account($email, $password);
							$result = $account->login();
							
							if($result >= 0)
							{
								if($result > 0)
								{
									$_SESSION["account"] = serialize($account);
									$_SESSION["userid"] = $result;
									$_SESSION["email"] = $email;
									
									echo "<p>Welcome back!";
									log_message("login", "Log-In from " . $_SERVER['REMOTE_ADDR'] . " @ " . $_SESSION["email"]);
								}
								else
								{
									echo "<p>Error! The login combination is not valid.</p>";
									log_message("login", "Failed login from " . $_SERVER['REMOTE_ADDR'] . " @ " . $email);
								}
							}
							else
							{
								echo "<p>Error! The account could not be found as registered.</p>";
							}
						}
						else if(isset($_POST["register"]))
						{
							echo '<h1>Register</h1>';
							$email = test_input($_POST['email']);
							$password = test_input($_POST['password']);
							$repeat = test_input($_POST['repeat']);
							$account = new Account($email, $password);
							
							if($password != $repeat)
							{
								echo "<p>Provided passwords do not match, please try again.</p>";
							}
							else if($account->register())
							{
								echo "<p>Registration has been completed successfully.</p>";
								log_message("register", "Account from " . $_SERVER['REMOTE_ADDR'] . " @ " . $email);
							}
							else
							{
								echo "<p>Error! Could not create the account as it already exists.</p>";
							}
						}
						else if(!isset($_GET["register"]))
						{
							echo '<h1>Log-In</h1>
							<p>Provide your credentials in order to access the ticket system.</p>
							<form action="account.php" method="post">
							  <div class="form_settings">
								<p><span>E-Mail</span><input class="contact" type="email" name="email" value="" /></p>
								<p><span>Password</span><input class="contact" type="password" name="password" value="" /></p>
								<p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="login" value="submit" /></p>
							  </div>
							</form>
							<p>Do not have an account? Click <a href="?register">here</a> to register yourself.</p>';
						}
						else
						{
							echo '<h1>Register</h1>
							<p>Here you can register your account used to access the ticket system.</p>
							<form action="account.php" method="post">
							  <div class="form_settings">
								<p><span>E-Mail</span><input class="contact" type="email" name="email" value="" /></p>
								<p><span>Password</span><input class="contact" type="password" name="password" value="" /></p>
								<p><span>Repeat</span><input class="contact" type="password" name="repeat" value="" /></p>
								<p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="register" value="submit" /></p>
							  </div>
							</form>';
						}
					}
					else
					{
						if(isset($_GET["logout"]))
						{
							log_message("login", "Log-Out from " . $_SERVER['REMOTE_ADDR'] . " @ " . $_SESSION["email"]);
							echo '<h1>See you later!</h1><p>You have been logged out.</p>';

							$account = unserialize($_SESSION["account"]);
							$account->logout();
						}
						else
						{
							echo '<h1>Welcome back!</h1><p>Press <a href="?logout">here</a> to log out.</p>';
						}
					}

					$conn->close();
				?>
				</div>
			</div>
			
			<?php include 'pages/footer.php'; ?>
		</div>
	</body>
</html>