<!DOCTYPE HTML>
<html>
	<head>
	  <title>QuickHelp - Home</title>
	  <?php include 'pages/meta_heading.php'; ?>
	</head>

	<body>
		<div id="main">
			<?php include 'pages/header.php'; ?>
			
			<div id="site_content">
				<?php include 'pages/sidebar.php'; ?>
				
				<div id="content">
					<!-- insert the page content here -->
					<h1>Welcome to the QuickHelp support system website!</h1>
					<p>Our solution will give an option for small firms to incorporate a support system into their companies.</p>
					<p>IT based support works based on ticket or live chat within a website or email based conversations. The client comes, submits a ticket or initiates a live conversation with an online member from the support team. Once the ticket arrives to a member the member than initiates a simple research into the problem and proposes solutions for the customers problem.
					<p>If the problem is solved at the clients side there is no need for further investigation and we file the ticket as complete. If the problem is company sided, internal investigations will be started and  the initial ticket will be transferred to a higher level.</p>
					<h2>Browser Compatibility</h2>
					<p>This template has been tested in the following browsers:</p>
					<ul>
						<li>Internet Explorer 8</li>
						<li>Internet Explorer 7</li>
						<li>FireFox 3.5</li>
						<li>Google Chrome 6</li>
						<li>Safari 4</li>
					</ul>
				</div>
			</div>
			
			<?php include 'pages/footer.php'; ?>
		</div>
	</body>
</html>