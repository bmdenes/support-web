<?php
	$conn = createConnection();
	$sql = "SELECT id, name, phone, address, description FROM companies ORDER BY id DESC LIMIT 3";
	$result = $conn->query($sql);
	
	if($result->num_rows > 0)
	{
		// output data of each row
		echo '<ul>';
		while($row = $result->fetch_assoc())
		{
			echo '<li>' . $row["name"]. ' - Support: <a href="tickets.php?id=' . $row["id"]. '">Tickets</a></li>';
		}
		echo '</ul>';
	}
	else echo '<p>None.</p>';
?>