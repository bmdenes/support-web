<div id="header">
  <div id="logo">
	<h1>QuickHelp<a href="/<?php echo basename(getcwd()); ?>">System</a></h1>
	<div class="slogan">Helps you to help others.</div>
  </div>
  <div id="menubar">
	<ul id="menu">
	  <!-- put class="current" in the li tag for the selected page - to highlight which page you're on -->
	  <?php
		require 'includes/mysql.php';
	
		$page = explode('?', basename($_SERVER['REQUEST_URI']))[0];
		if($page == basename(getcwd())) $page = 'index.php';;
		
		$pages = array('index.php', 'account.php', 'register.php', 'tickets.php', 'chat.php', 'contact.php');
		$titles = array('Home', 'Account', 'Company', 'Tickets', 'Live Support', 'Contact Us');
		
		for($i = 0; $i < count($pages); $i++)
		{
			echo '<li ';
			if($page == $pages[$i]) echo 'class="current"';
			echo '><a href="' . $pages[$i] . '">' . $titles[$i] . '</a></li>';
		}
	  ?>
	</ul>
  </div>
</div>