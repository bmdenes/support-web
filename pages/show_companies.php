<?php
	$limit = explode('?', basename($_SERVER['REQUEST_URI']))[0] == "register.php" ? 3 : 10;
	$page = (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
		
	$conn = createConnection();
	$sql = "SELECT id, name, phone, address, description FROM companies LIMIT $limit OFFSET " . ($page - 1) * $limit;
	$result = $conn->query($sql);
	
	if($result->num_rows > 0)
	{
		// output data of each row
		echo '<table>';
		while($row = $result->fetch_assoc())
		{
			echo '<tr><td>' . $row["name"]. '</td>
			<td>' . substr($row["description"], 0, 30) . '...</td>
			<td><a href="tickets.php?id=' . $row["id"]. '">Tickets</a></td></tr>';
		}
		echo '</table>';
	}
	else echo '<p>None.</p>';
	
	$cnt = $conn->query("SELECT COUNT(1) AS Count FROM companies")->fetch_assoc()["Count"];
	if($cnt > 0)
	{
		echo '<p>Page: &nbsp;';
		for($i = 1; $i <= floor((int)$cnt / $limit) + 1; $i++)
		{
			if($i == $page) echo $i . " &nbsp;";
			else echo '<a href="?page=' . $i . '">' . $i . '</a> &nbsp;';
		}
		echo '</p>';
	}
?>