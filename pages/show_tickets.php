<?php
	$limit = 5;
	$page = (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1;
	
	$conn = createConnection();
	$sql = "SELECT id, companyid, email, title, date, description FROM tickets WHERE companyid = $id
			ORDER BY id DESC LIMIT $limit OFFSET " . ($page - 1) * $limit;
	$result = $conn->query($sql);
	
	if($result->num_rows > 0)
	{
		// output data of each row
		echo '<table>';
		while($row = $result->fetch_assoc())
		{
			echo '<tr><td style="width: 110px">' . $row["title"] . '</td>
			<td>' . substr($row["description"], 0, 30) . '...</td>
			<td>' . $row["date"] . '</td>
			<td><a href = "?view=' . $row["id"] . '">View</a></td></tr>';
		}
		echo '</table>';
	}
	else echo '<p>None.</p>';
	
	$cnt = $conn->query("SELECT COUNT(1) AS Count FROM tickets WHERE companyid = $id")->fetch_assoc()["Count"];
	if($cnt > 0)
	{
		echo '<p>Page: &nbsp;';
		for($i = 1; $i <= floor((int)$cnt / $limit) + 1; $i++)
		{
			if($i == $page) echo $i . " &nbsp;";
			else echo '<a href="'. $_SERVER['REQUEST_URI'] .'&page=' . $i . '">' . $i . '</a> &nbsp;';
		}
		echo '</p>';
	}
?>