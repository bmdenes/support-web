<div id="sidebar_container">
	<img class="paperclip" src="style/images/paperclip.png" alt="paperclip" />
	<div class="sidebar">
		<h3>Latest Companies</h3>
		<?php include 'pages/latest_companies.php' ?>
	</div>
	<img class="paperclip" src="style/images/paperclip.png" alt="paperclip" />
	<div class="sidebar">
		<h3>Latest Tickets</h3>
		<?php include 'pages/latest_tickets.php' ?>
	</div>
	<img class="paperclip" src="style/images/paperclip.png" alt="paperclip" />
	<div class="sidebar">
		<h3>Newsletter</h3>
		<p>If you would like to receive our newletter, please enter your email address and click 'Subscribe'.</p>
		<form method="post" action="#" id="subscribe">
			<p style="padding: 0 0 9px 0;"><input class="search" type="text" name="email_address" value="Your e-mail address..." onclick="javascript: document.forms['subscribe'].email_address.value=''" /></p>
			<p><input class="subscribe" name="subscribe" type="submit" value="Subscribe" /></p>
		</form>
	</div>
</div>