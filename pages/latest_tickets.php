<?php
	$conn = createConnection();
	$sql = "SELECT id, companyid, email, title, description FROM tickets ORDER BY id DESC LIMIT 3";
	$result = $conn->query($sql);
	
	if($result->num_rows > 0)
	{
		// output data of each row
		echo '<ul>';
		while($row = $result->fetch_assoc())
		{
			echo '<li>' . $row["title"] . '</li>';
		}
		echo '</ul>';
	}
	else echo '<p>None.</p>';
?>