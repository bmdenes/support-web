<?php session_start(); ?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>QuickHelp - Tickets</title>
		<?php include 'pages/meta_heading.php'; ?>
	</head>

	<body>
		<div id="main">
			<?php include 'pages/header.php'; ?>
			
			<div id="site_content">
				<?php include 'pages/sidebar.php'; ?>
				
				<div id="content">
					<?php
						require 'includes/utils.php';
						require 'includes/Ticket.php';
						require 'includes/Message.php';
						
						$conn = createConnection();
						
						if(isset($_GET['view']) && is_numeric($_GET['view']) && $_GET['view'] > 0)
						{
							echo '<h1>Viewing Ticket</h1>';
							$id = test_input($_GET['view']);
							
							if(isset($_POST['message']))
							{
								if(isset($_SESSION['userid']))
								{
									$userid = $_SESSION["userid"];
									$text = test_input($_POST['description']);
									$message = new Message($userid, $id, $text);

									if($message->submit())
									{
										echo "<p>Message has been submitted successfully.</p>";
										log_message("tickets", "Message from " . $_SESSION["email"] . " @ Ticket " . $id);
									}
									else
									{
										echo "<p>Error! Could not connect to the database.</p>";
									}
								}
								else
								{
									echo "<p>Error! You need to be logged in to submit a message.</p>";
								}
							}
							else
							{
								$sql = "SELECT id, companyid, userid, email, title, date, description FROM tickets WHERE id = $id";
								$result = $conn->query($sql);
								
								if($result->num_rows > 0)
								{
									$row = $result->fetch_assoc();
									
									echo '<form>
										<div class="form_settings">
											<p><span>Title</span><input disabled="disabled" class="contact" type="title" name="title" value="' . $row['title'] . '"></p>
											<p><span>Description</span><textarea disabled="disabled" class="contact textarea" rows="8" cols="50" name="description">'. $row['description'] .'</textarea>
											<p><span>Date</span><input disabled="disabled" class="contact" type="date" name="date" value="' . $row['date'] . '"></p>
										</div>
									</form>
									<h1 style="margin-top: 75px">Submit Answer</h1>
									<form action="tickets.php?view=' . $id . '" method="post">
										<div class="form_settings">
											<p><span>Message</span><textarea class="contact textarea" rows="4" cols="50" name="description"></textarea>
											<p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="message" value="Submit" /></p>
										</div>
									</form>';
									
									$sql = "SELECT COUNT(1) as Count FROM messages WHERE ticketid = $id";
									echo '<p>There are currently ' . $conn->query($sql)->fetch_assoc()["Count"] . ' answers to this ticket.</p>';
								}
								else
								{
									echo '<p>Error! The ticket could not be found.</p>';
								}
							}
						}
						else if(isset($_GET['id']))
						{
							$id = test_input($_GET['id']);
							
							$sql = "SELECT id, name, phone, address, description FROM companies WHERE id = $id";
							$result = $conn->query($sql);
							
							if($result->num_rows > 0)
							{
								$name = $result->fetch_assoc()['name'];
								echo '<h1>Create ticket for ' . $name . ':</h1>';
								
								if(isset($_POST['ticket']))
								{
									if(isset($_SESSION["userid"]))
									{
										$userid = $_SESSION["userid"];
										$email = $_SESSION["email"];
										$title = test_input($_POST['title']);
										$description = test_input($_POST['description']);
										$ticket = new Ticket($id, $userid, $email, $title, $description);

										if($ticket->submit())
										{
											echo "<p>Ticket has been submitted successfully.</p>";
											log_message("tickets", "Ticket from " . $email . " @ " . $name);
										}
										else
										{
											echo "<p>Error! Could not connect to the database.</p>";
										}
									}
									else
									{
										echo "<p>Error! You need to be logged in to create a ticket.</p>";
									}
								}
								else
								{
									$email = isset($_SESSION["email"]) ? $_SESSION["email"] : "You need to login first...";
									
									echo '<p>Here you can create a ticket addressed towards this company.</p>
										<form action="tickets.php?id=' . $id . '" method="post">
											<div class="form_settings">
												<p><span>E-Mail</span><input disabled="disabled" class="contact" type="email" name="email" value="' . $email . '"></p>
												<p><span>Title</span><input class="contact" type="title" name="title"></p>
												<p><span>Description</span><textarea class="contact textarea" rows="8" cols="50" name="description"></textarea>
												<p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="ticket" value="Create" /></p>
											</div>
										</form>';
								}
								
								echo '<h1>Submitted tickets:</h1>';
								include 'pages/show_tickets.php';
							}
							else echo '<h1>The specific ticket system could not be found.</h1>';
						}
						else
						{
							echo '<h1>Registered companies:</h1>
							<p>Here are the companies that have already been registered into the system.</p>';
							include 'pages/show_companies.php';
						}
						
						$conn->close();
					?>
				</div>
			</div>
			
			<?php include 'pages/footer.php'; ?>
		</div>
	</body>
</html>