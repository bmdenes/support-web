<?php class Ticket
{
    private $conn, $ticketid = 0, $companyid, $userid;
    private $email, $title, $description;

    public function getTicketID()
    {
        return $this->ticketid;
    }

    public function getCompanyID()
    {
        return $this->companyid;
    }

    public function getUserID()
    {
        return $this->userid;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function isSubmitted()
    {
        return $this->getTicketID() > 0;
    }

    function __construct($companyid, $userid, $email, $title, $description)
    {
        $this->companyid = $companyid;
        $this->userid = $userid;
        $this->email = $email;
        $this->title = $title;
        $this->description = $description;

        $this->conn = createConnection();
    }

    function submit()
    {
        $sql = "INSERT INTO tickets (companyid, userid, email, title, date, description) VALUES ('$this->companyid', $this->userid, '$this->email', '$this->title', NOW(), '$this->description')";
        mysql_real_escape_string($sql);
        $result = $this->conn->query($sql);

        if($result)
        {
            $this->ticketid = $this->conn->insert_id;
        }

        return $result;
    }
}