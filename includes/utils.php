<?php
	function test_include_utils()
	{
		return true;
	}

	function test_input($data)
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	
	function log_message($name, $message, $debug = false)
	{
		$path = 'logs/' . $name . '/';
		$date = str_replace(' ', '_', date('Y m d'));
		$fullPath = $path . $date . '.log';
		
		if(!file_exists($path))
		{
			mkdir($path, 0777, true);
		}
		
		if($logFile = fopen($fullPath, 'a'))
		{
			$text = '[' . date('H:i:s').']: ' . $message . "\r\n";
			
			fwrite($logFile, $text);
			fclose($logFile);
		}

		if($debug) return $fullPath;
	}
?>