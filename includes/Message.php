<?php class Message
{
    private $conn, $messageid = 0;
    private $userid, $ticketid, $text;

    public function getMessageID()
    {
        return $this->messageid;
    }

    public function getUserID()
    {
        return $this->userid;
    }

    public function getTicketID()
    {
        return $this->ticketid;
    }

    public function getText()
    {
        return $this->text;
    }

    public function isAdded()
    {
        return $this->getMessageID() > 0;
    }

    function __construct($userid, $ticketid, $text)
    {
        $this->userid = $userid;
        $this->ticketid = $ticketid;
        $this->text = $text;

        $this->conn = createConnection();
    }

    function submit()
    {
        $sql = "INSERT INTO messages (userid, ticketid, message) VALUES ('$this->userid', '$this->ticketid', '$this->text')";
        mysql_real_escape_string($sql);
        $result = $this->conn->query($sql);

        if($result)
        {
            $this->messageid = $this->conn->insert_id;
        }

        return $result;
    }
}