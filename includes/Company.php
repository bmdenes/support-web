<?php class Company
{
    private $companyid = 0, $conn;
    private $name, $phone, $address, $description;

    public function getName()
    {
        return $this->name;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getCompanyID()
    {
        return $this->companyid;
    }

    public function isRegistered()
    {
        return $this->getCompanyID() > 0;
    }

    function __construct($name, $phone, $address, $description)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->address = $address;
        $this->description = $description;

        $this->conn = createConnection();
    }

    function register()
    {
        $sql = "INSERT INTO companies (name, phone, address, description) VALUES ('$this->name', '$this->phone', '$this->address', '$this->description')";
        mysql_real_escape_string($sql);
        $result = $this->conn->query($sql);

        if($result)
        {
            $this->companyid = $this->conn->insert_id;
        }

        return $result;
    }
}