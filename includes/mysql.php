<?php
	function test_include_database()
	{
		return true;
	}


	function createConnection()
	{
		if(isset($conn)) return $conn;
		
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "support";
		
		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		
		// Check connection
		if($conn->connect_error)
		{
			die("Connection failed: " . $conn->connect_error);
		}
		
		return $conn;
	}
?>