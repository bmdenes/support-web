<?php class Account
{
    private $userid = 0, $conn;
    private $email, $password;

    function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;

        $this->conn = createConnection();
    }

    public function getUserID()
    {
        return $this->userid;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function isLoggedIn()
    {
        return $this->getUserID() > 0;
    }

    function register()
    {
        $hash = md5(md5($this->password) + "be_salty");
        $sql = "INSERT INTO users (email, password) VALUES ('$this->email', '$hash')";
        mysql_real_escape_string($sql);

        return $this->conn->query($sql);
    }

    function login()
    {
        $hash = md5(md5($this->password) + "be_salty");
        $sql = "SELECT id, email, password FROM users WHERE email = '$this->email'";
        $result = $this->conn->query($sql);

        if($result->num_rows > 0)
        {
            $row = $result->fetch_assoc();

            if($hash == $row["password"])
            {
                $this->userid = $row["id"];
                return $this->getUserID();
            }
            else return 0;
        }
        else return -1;
    }

    function logout()
    {
        session_unset();

        $this->userid = 0;
        $this->email = $this->password = "";
    }
}