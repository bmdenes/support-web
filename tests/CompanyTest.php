<?php class CompanyTest extends PHPUnit_Framework_TestCase
{
    public function testCompanyInclude()
    {
        require_once 'includes/Company.php';
        require_once 'includes/mysql.php';

        $company = new Company("Test", "", "", "");
        $this->assertEquals("Test", $company->getName());
    }

    public function testCompanyRegister()
    {
        $company = new Company("Register", "", "", "");
        $this->assertEquals(true, $company->register());
    }

    public function testIsCompanyRegistered()
    {
        $company = new Company("Registered", "", "", "");
        $this->assertEquals(false, $company->isRegistered());

        $company->register();
        $this->assertEquals(true, $company->isRegistered());
    }
}