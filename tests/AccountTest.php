<?php class AccountTest extends PHPUnit_Framework_TestCase
{
    public function testAccountInclude()
    {
        require_once 'includes/Account.php';
        require_once 'includes/mysql.php';

        $account = new Account("Test", "Password");
        $this->assertEquals("Test", $account->getEmail());
    }

    public function testAccountRegister()
    {
        $account = new Account("test@test.dk", "test");
        $this->assertEquals(false, $account->register());
    }

    public function testAccountLogin()
    {
        $account = new Account("test@test.dk", "test"); // Success
        $this->assertGreaterThan(0, $account->login());

        $account = new Account("test@test.dk", ""); // Wrong Password
        $this->assertEquals(0, $account->login());

        $account = new Account("test", "test"); // No Account
        $this->assertLessThan(0, $account->login());
    }

    public function testAccountLogout()
    {
        $account = new Account("test@test.dk", "test");

        $account->login();
        $this->assertEquals(true, $account->isLoggedIn());

        $account->logout();
        $this->assertEquals(false, $account->isLoggedIn());
    }
}