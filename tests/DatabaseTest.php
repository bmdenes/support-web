<?php class DatabaseTest extends PHPUnit_Framework_TestCase
{
    public function testDatabaseInclude()
    {
        require_once 'includes/mysql.php';

        $this->assertEquals(true, test_include_database());
    }

    public function testConnectionCreate()
    {
        $conn = createConnection();

        $this->assertEquals(false, $conn->connect_error);
    }
}