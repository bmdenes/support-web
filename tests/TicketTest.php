<?php class TicketTest extends PHPUnit_Framework_TestCase
{
    public function testTicketInclude()
    {
        require_once 'includes/Ticket.php';
        require_once 'includes/mysql.php';

        $ticket = new Ticket(123, 0, "", "", "");
        $this->assertEquals(123, $ticket->getCompanyID());
    }

    public function testTicketSubmit()
    {
        $ticket = new Ticket(123, 0, "", "Submit", "");
        $this->assertEquals(true, $ticket->submit());
    }

    public function testTicketSubmitted()
    {
        $ticket = new Ticket(123, 0, "", "Submitted", "");
        $this->assertEquals(false, $ticket->isSubmitted());

        $ticket->submit();
        $this->assertEquals(true, $ticket->isSubmitted());
    }
}