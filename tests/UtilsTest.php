<?php class UtilsTest extends PHPUnit_Framework_TestCase
{
    public function testUtilsInclude()
    {
        require_once 'includes/utils.php';

        $this->assertEquals(true, test_include_utils());
    }

    public function testInputFormat()
    {
        $normalInput = "normal string";
        $dirtyInput = "\d\irt\y \str\in\g"; // = "dirty string"

        $normalTest = test_input($normalInput);
        $dirtyTest = test_input($dirtyInput);

        $this->assertEquals($normalInput, $normalTest);
        $this->assertEquals("dirty string", $dirtyTest);
    }

    public function testMessageLog()
    {
        $testFile = log_message("tests", "Test message...", true);

        $this->assertEquals(true, file_exists($testFile));
    }
}