<?php class MessageTest extends PHPUnit_Framework_TestCase
{
    public function testMessageInclude()
    {
        require_once 'includes/Message.php';
        require_once 'includes/mysql.php';

        $message = new Message(123, 123, "");
        $this->assertEquals(123, $message->getUserID());
    }

    public function testMessageSubmit()
    {
        $message = new Message(123, 123, "Add");
        $this->assertEquals(true, $message->submit());
    }

    public function testMessageSubmitted()
    {
        $message = new Message(123, 123, "Added");
        $this->assertEquals(false, $message->isAdded());

        $message->submit();
        $this->assertEquals(true, $message->isAdded());
    }
}