<!DOCTYPE HTML>
<html>
	<head>
	  <title>QuickHelp - Contact</title>
	  <?php include 'pages/meta_heading.php'; ?>
	</head>

	<body>
		<div id="main">
			<?php include 'pages/header.php'; ?>
			
			<div id="site_content">
				<?php include 'pages/sidebar.php'; ?>
				
				<div id="content">
					<h1>Contact Us</h1>
					<p>Here you can contact us for any questions regarding the system:</p>
					<form action="#" method="post">
					  <div class="form_settings">
						<p><span>Name</span><input class="contact" type="name" name="name" value="" /></p>
						<p><span>E-Mail</span><input class="contact" type="email" name="email" value="" /></p>
						<p><span>Message</span><textarea class="contact textarea" rows="8" cols="50" name="message"></textarea></p>
						<p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact" value="submit" /></p>
					  </div>
					</form>
				</div>
			</div>
			
			<?php include 'pages/footer.php'; ?>
		</div>
	</body>
</html>